# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git init
git remote add origin https://gl9268@bitbucket.org/gl9268/stroboskop.git
git fetch
git checkout master
```

Naloga 6.2.3:
https://bitbucket.org/gl9268/stroboskop/commits/7d7fbfef1c7ae56d4937aaeb79b69ca49fe5467a

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/gl9268/stroboskop/commits/c0273761041cf38c4a83cddc529b6f705b595c07

Naloga 6.3.2:
https://bitbucket.org/gl9268/stroboskop/commits/8bcffd7c1b88147e49c820f883baf628197b73f4

Naloga 6.3.3:
https://bitbucket.org/gl9268/stroboskop/commits/81e74bfbffc1194170ecc1c78086e66125ac5d24

Naloga 6.3.4:
https://bitbucket.org/gl9268/stroboskop/commits/c7fb9decbaf66f4bb4a00a52987f7b908447ecbd

Naloga 6.3.5:

```
git checkout master
git merge izgled --no-ff
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/gl9268/stroboskop/commits/6195ef6fbbd099c890f7b59de86a13df0a118b3e

Naloga 6.4.2:
https://bitbucket.org/gl9268/stroboskop/commits/20696a335a5212f84f27c0c3ebd86d4d979e2ad4

Naloga 6.4.3:
https://bitbucket.org/gl9268/stroboskop/commits/d41b22b7578b78515174c61f6984775415915cee

Naloga 6.4.4:
https://bitbucket.org/gl9268/stroboskop/commits/516f743cdc566c51fc2e5d6a8c49aba399a8864c